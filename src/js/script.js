$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    items: 1,
    responsive: {
      0: {
        nav: false,
      },
      992: {
        loop: true,
      }
    }
  });

  $('.testimonials__slider').owlCarousel({
    items: 3,
    dots: false,
    margin: 30,
    nav: true,
    navText: ['<svg width="20" height="16"><use xlink:href="#arrow-left"></use></svg>', '<svg width="20" height="16"><use xlink:href="#arrow-right"></use></svg>'],
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      },
      992: {
        items: 3,
      }
    }
  });
});
